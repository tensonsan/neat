
#ifndef _GA_H_
#define _GA_H_

#include "types.h"

#define NUM_CHROMOSOMES 9

/// Chromosome
typedef struct 
{
    flt32_t value[NUM_CHROMOSOMES]; ///< Chromosome values
    flt32_t fitness;                ///< Fitness value
} ga_chromosome_t;

/// Genetic algorithm handle
typedef struct
{
    ga_chromosome_t *population;        ///< Current generation population
    ga_chromosome_t *population_new;    ///< Next generation population
    int32_t n_population;               ///< Length of population and population_new
    flt32_t crossover_rate;             ///< Crossover rate in range [0, 1]
    flt32_t value_scale;                ///< Population value scale factor
    flt32_t p_crossover;                ///< Crossover probability [0, 1]
    flt32_t p_mutation;                 ///< Mutation probability [0, 1]
    flt32_t (*fitness)(void *arg, int32_t member);  ///< Fitness function
    int32_t n_iterations;               ///< Maximal number of iterations
    flt32_t avg_fitness_target;         ///< Average fitness target value
} ga_t;

void ga_init(ga_t *ga);
int32_t ga_process(ga_t *ga);

#endif