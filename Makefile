
CC=gcc
TARGET=test
SOURCES=main.c ga.c mlp.c
CFLAGS=-O0
LFLAGS=-lm

# define list of objects
OBJS=$(SOURCES:.c=.o)

# the target is obtained linking all .o files
all: $(SOURCES) $(TARGET)

$(TARGET): $(OBJS)
	$(CC) $(CFLAGS) $(OBJS) -o $(TARGET) $(LFLAGS)

purge: clean
	rm -f $(TARGET)

clean:
	rm -f *.o
	rm -f $(TARGET)