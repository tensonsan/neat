
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <float.h>
#include "ga.h"

/// Maps v real value from [0, 1] to [-1, 1] range
#define GA_MAP(x)   (2.0f * (x) - 1.0f)

/**
 * @brief Returns real random value
 * 
 * @return Real random value in range [0, 1]
 */
static flt32_t ga_randf(void)
{
    return (flt32_t)rand() / (flt32_t)RAND_MAX;
}

/**
 * @brief Performs a biased coin toss
 * 
 * @param[in] probability Heads probability
 * 
 * @retval 1 if heads, 0 if tails
 */
static flt32_t ga_coin_toss(float probability)
{
    if (ga_randf() < probability)
    {
        return 1;
    }

    return 0;
}

/**
 * @brief Performs tournament parent selection
 * 
 * @param[in] ga Genetic algorithm handle
 * @param[out] first First parent index in population
 * @param[out] second Second parent index in population
 */
static void ga_tournament(ga_t *ga, int32_t *first, int32_t *second)
{
    int32_t _first = 0, _second = 0;

    for (int32_t i=0; i < ga->n_population; ++i)
    {
        int32_t index = rand() % ga->n_population;
        if (ga->population[index].fitness > ga->population[_first].fitness)
        {
            _second = _first;
            _first = index;
        }
    }

    *first = _first;
    *second = _second;
}

/**
 * @brief Updates population fitness
 * 
 * @param[in] ga Genetic algorithm handle
 *
 * @return Average fitness value
 */
static flt32_t ga_update_fitness(ga_t *ga)
{
    flt32_t sum = 0.0f;

    for (int32_t i=0; i < ga->n_population; ++i)
    {
        ga->population[i].fitness = ga->fitness(ga, i);
        sum += ga->population[i].fitness;
    }

    return sum / (flt32_t)ga->n_population;
}

/**
 * @brief Performs a uniform crossover with \ref p_crossover probability
 * 
 * @param[in] ga Genetic algorithm handle
 */
static void ga_crossover(ga_t *ga)
{
    for (int32_t i=0; i < ga->n_population; i+=2)
    {
        int32_t p1 = 0, p2 = 0;
        ga_tournament(ga, &p1, &p2);

        for (int32_t j=0; j < NUM_CHROMOSOMES; ++j)
        {
            if (ga_coin_toss(ga->p_crossover))
            {
                ga->population_new[i].value[j] = ga->crossover_rate * ga->population[p1].value[j] + (1.0f - ga->crossover_rate) * ga->population[p2].value[j];
                ga->population_new[i+1].value[j] = ga->crossover_rate * ga->population[p2].value[j] + (1.0f - ga->crossover_rate) * ga->population[p1].value[j];
            }
            else
            {
                ga->population_new[i].value[j] = ga->population[p1].value[j];
                ga->population_new[i+1].value[j] = ga->population[p2].value[j];
            }
        }
    }
}

/**
 * @brief Performs a shrink mutation with \ref p_mutation probability
 * 
 * @param[in] ga Genetic algorithm handle
 */
static void ga_mutation(ga_t *ga)
{
    for (int32_t i=0; i < ga->n_population; ++i)
    {
        for (int32_t j=0; j < NUM_CHROMOSOMES; ++j)
        {
            if (ga_coin_toss(ga->p_mutation))
            {
                ga->population[i].value[j] = GA_MAP(ga_randf()) * ga->value_scale + ga->population_new[i].value[j];
            }
            else
            {
                ga->population[i].value[j] = ga->population_new[i].value[j];
            }
        }
    }
}

/**
 * @brief Initializes genetic algorithm
 * 
 * @param[in] ga Genetic algorithm handle
 */
void ga_init(ga_t *ga)
{
    srand(time(NULL));

    for (int32_t i=0; i < ga->n_population; ++i)
    {
        for (int32_t j=0; j < NUM_CHROMOSOMES; ++j)
        {
            ga->population[i].value[j] = ga->value_scale * GA_MAP(ga_randf());
        }
        ga->population[i].fitness = 0.0f;
    }
}

/**
 * @brief Displays a single generation statistics
 * 
 * @param[in] ga Genetic algorithm handle
 * @param[in] iteration Generation index
 * @param[in] avg_fitness Average fitness value
 */
void ga_print(ga_t *ga, int32_t iteration, flt32_t avg_fitness)
{
    printf("%d, %f\n", iteration, avg_fitness);
#if 0
    for (int32_t i=0; i < ga->n_population; ++i)
    {
        printf(" ");
        for (int32_t j=0; j < NUM_CHROMOSOMES; ++j)
        {
            printf("%f, ", ga->population[i].value[j]);
        }
        printf("(%f)\n", ga->population[i].fitness);
    }
#endif
}

/**
 * @brief Processes genetic algorithm
 * 
 * @param[in] ga Genetic algorithm handle
 */
int32_t ga_process(ga_t *ga)
{
    for (int32_t i=0; i < ga->n_iterations; ++i)
    {
        flt32_t avg_fitness = ga_update_fitness(ga);
        if (avg_fitness >= ga->avg_fitness_target)
        {
            return 1;
        }

        ga_print(ga, i, avg_fitness);

        ga_crossover(ga);

        ga_mutation(ga);
    }

    return 0;
}