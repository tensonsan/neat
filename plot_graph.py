
import numpy as np
import matplotlib.pyplot as plt

my_data = np.genfromtxt('dump.csv', delimiter=',', skip_header=0)

plt.title('GA convergence')
plt.plot(my_data[:,1])
plt.xlabel('iteration')
plt.ylabel('fitness')
plt.grid()
plt.show()