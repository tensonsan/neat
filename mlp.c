
#include "mlp.h"

/**
 * @brief Computes single perceptron layer output
 * 
 * @param[in] layer Layer handle 
 * @param[in] x Input values 
 * @param[out] y Output values
 */
void mlp_layer(mlp_layer_t *layer, flt32_t *x, flt32_t *y)
{
    for (int32_t j=0; j < layer->n_outputs; ++j)
    {
        flt32_t sum = layer->bias[j];

        for (int32_t i=0; i < layer->n_inputs; ++i)
        {
            sum += x[i] * layer->weight[i + j * layer->n_inputs];
        }

        y[j] = layer->activation(sum);
    }
}