
#ifndef _MLP_H_
#define _MLP_H_

#include "types.h"

/// Multilayer perceptron layer
typedef struct 
{
    const flt32_t *weight;      ///< Weight array (dimension n_inputs * n_outputs)
    const flt32_t *bias;        ///< Bias array (dimension n_outputs)
    int32_t n_inputs;           ///< Number of inputs
    int32_t n_outputs;          ///< Number of outputs
    flt32_t (*activation)(flt32_t x);   ///< Activation function
} mlp_layer_t;

void mlp_layer(mlp_layer_t *layer, flt32_t *x, flt32_t *y);

#endif
