
#include <stdio.h>
#include <math.h>
#include "ga.h"
#include "mlp.h"

#define POPULATION_SIZE 200

ga_chromosome_t population[POPULATION_SIZE];
ga_chromosome_t population_new[POPULATION_SIZE];
ga_t ga;

#define LAYER1_INPUTS   2
#define LAYER1_OUTPUTS  2

#define LAYER2_INPUTS   2
#define LAYER2_OUTPUTS  1

flt32_t bias1[LAYER1_OUTPUTS];
flt32_t weight1[LAYER1_INPUTS * LAYER1_OUTPUTS];
mlp_layer_t mlp1;

flt32_t bias2[LAYER2_OUTPUTS];
flt32_t weight2[LAYER2_INPUTS * LAYER2_OUTPUTS];
mlp_layer_t mlp2;

flt32_t middle_layer[LAYER1_OUTPUTS];
flt32_t output_layer[LAYER2_OUTPUTS];

flt32_t activation(flt32_t x)
{
    return x > 0.0f ? x : 0.01f * x;
}

#define TRAIN_SAMPLES 4

flt32_t training_set_inputs[TRAIN_SAMPLES][LAYER1_INPUTS] = {{0.0f, 0.0f}, {0.0f, 1.0f}, {1.0, 0.0f}, {1.0f, 1.0f}};
flt32_t training_set_outputs[TRAIN_SAMPLES] = {0.0f, 1.0f, 1.0f, 0.0f};

void update_weights(int32_t member)
{
    weight1[0] = population[member].value[0];
    weight1[1] = population[member].value[1];
    weight1[2] = population[member].value[2];
    weight1[3] = population[member].value[3];

    bias1[0] = population[member].value[4];
    bias1[1] = population[member].value[5];

    weight2[0] = population[member].value[6];
    weight2[1] = population[member].value[7];

    bias2[0] = population[member].value[8];
}

void update_weights_reference(void)
{
    weight1[0] = -1.67987976f;
    weight1[1] = 1.74564025f;
    weight1[2] = 1.67951616f;
    weight1[3] = -1.74569812;

    bias1[0] = -2.25374265e-4f;
    bias1[1] = 7.33762948e-5f;

    weight2[0] = 2.99636229f;
    weight2[1] = 2.30751166f;

    bias2[0] = -1.70963077f;
}

void mlp_forward(flt32_t *x, flt32_t *y)
{
        mlp_layer(&mlp1, x, middle_layer);
        mlp_layer(&mlp2, middle_layer, y);

        y[0] = y[0] > 0.5f ? 1.0f : 0.0f;
}

flt32_t fitness(void *arg, int32_t member)
{
    ga_t *pga = (ga_t *)arg;

    update_weights(member);

    flt32_t rv = 0.0f;

    for (int32_t j=0; j < TRAIN_SAMPLES; ++j)
    {
        mlp_forward(&training_set_inputs[j][0], &output_layer[0]);

        rv += fabs(output_layer[0] - training_set_outputs[j]);
    }

    rv /= (flt32_t)TRAIN_SAMPLES;

    // GA algorithm tries to maximize the fitness function
    rv = 1.0f - rv;

    return rv;
}

int main(int argc, char *argv[])
{
    ga.avg_fitness_target = 0.9f;
    ga.n_iterations = 10000;
    ga.crossover_rate = 0.5f;
    ga.value_scale = 100.0f;
    ga.p_crossover = 0.66f;
    ga.p_mutation = 0.01f;
    ga.population = population;
    ga.population_new = population_new;
    ga.n_population = POPULATION_SIZE;
    ga.fitness = fitness;

    mlp1.activation = activation;
    mlp1.bias = bias1;
    mlp1.weight = weight1;
    mlp1.n_inputs = LAYER1_INPUTS;
    mlp1.n_outputs = LAYER1_OUTPUTS;

    mlp2.activation = activation;
    mlp2.bias = bias2;
    mlp2.weight = weight2;
    mlp2.n_inputs = LAYER2_INPUTS;
    mlp2.n_outputs = LAYER2_OUTPUTS;

    ga_init(&ga);

    int32_t rv = ga_process(&ga);
    printf("convergence: %d\n", rv);

    printf("test:\n");
    //update_weights_reference();

    for (int32_t i=0; i < TRAIN_SAMPLES; ++i)
    {
        mlp_forward(&training_set_inputs[i][0], &output_layer[0]);

        printf("%f %f: %f\n", training_set_inputs[i][0], training_set_inputs[i][1], output_layer[0]);
    }

    return 0;
}